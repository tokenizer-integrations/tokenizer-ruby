require 'sinatra/base'

class FakeTokenizerApi < Sinatra::Base
  
  post '/v1/authentications.json' do
    valid_params.each do |k, v|
      params[k].to_s != v and return json_response 400, 'invalid_auth.json'
    end
    json_response 200, 'valid_auth.json'
  end

  get '/v1/authentication/:token_id.json' do
    valid_user.each do |k, v|
      params[k].to_s != v and return json_response 400, 'invalid_user.json'
    end
    if Time.now.to_i % 5 == 0
      json_response 200, 'valid_user.json'
    else
      json_response 200, 'pending_auth.json'
    end
  end

  private

    def json_response http_code, fixture_filename
      content_type :json
      status http_code
      File.open(File.dirname(__FILE__) + '/fixtures/' + fixture_filename, 'rb').read
    end

    def valid_params
      {
          app_id:     'fake_app',
          app_key:    'fake_key',
          usr_email:  'valid@example.com'
      }
    end

    def valid_user
      {
          app_id:     'fake_app',
          app_key:    'fake_key',
          token_id:   '666'
      }
    end
end