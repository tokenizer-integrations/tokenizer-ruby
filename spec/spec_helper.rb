require 'bundler/setup'
Bundler.setup

require 'support/fake_tokenizer_api'
require 'tokenizer2fa/client'

require 'capybara/rspec'
require 'webmock/rspec'
WebMock.disable_net_connect!(allow_localhost: true)

RSpec.configure do |config|
  config.before(:each) do
    stub_request(:any, /api.tokenizer.com/).to_rack(FakeTokenizerApi)
  end
end
