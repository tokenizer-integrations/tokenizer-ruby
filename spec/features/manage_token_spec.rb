require 'spec_helper'

tokenizer_config = {
    app_id:  'fake_app',
    app_key: 'fake_key',
    host:    'https://api.tokenizer.com/'
}
tokenizer = Tokenizer2fa::Client.new tokenizer_config

feature 'Validating configuration' do
  it 'succeeds only for a fully a configured instance of tokenizer connector' do
    expect(tokenizer.validate_config).to be_truthy
  end

  it 'fails when a user skips any of the params when initializing connector' do
    expect(Tokenizer2fa::Client.new.validate_config).to be_falsey
  end
end

feature 'Creating a token' do
  it 'requests Tokenizer API for a new token ID for e-mail address' do

    id = tokenizer.create_auth 'valid@example.com'
    expect(id).not_to be_falsey
  end

  it 'fails when provided invalid e-mail address' do

    id = tokenizer.create_auth 'not-an-email-address'
    expect(id).to be_nil
  end
end

feature 'Verifying a token' do
  it 'succeeds after at least one trial using valid token' do
    state = nil
    10.times do
      state = tokenizer.verify_auth '666'
      break unless state.is_pending?
      sleep 1
    end
    expect(state.is_accepted?).to be_truthy
  end

  it 'fails when using non-existing token_id' do
    state = nil
    10.times do
      state = tokenizer.verify_auth 'non-existing-token'
      break unless state.is_pending?
      sleep 1
    end
    expect(state.is_accepted?).to be_falsey
  end

  it 'never gives pending state when polling token verification' do
    state = tokenizer.poll_verify_auth '666'
    expect(state.is_pending?).to be_falsey

    state = tokenizer.poll_verify_auth 'non-existing-token'
    expect(state.is_pending?).to be_falsey
  end
end