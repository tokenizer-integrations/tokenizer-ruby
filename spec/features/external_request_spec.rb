require 'spec_helper'

feature 'External request' do
  it 'makes sure external HTTP requests are disabled' do
    uri = URI('https://tokenizer.com')

    expect {
      response = Net::HTTP.get(uri)
    }.to raise_error(WebMock::NetConnectNotAllowedError)
  end
end