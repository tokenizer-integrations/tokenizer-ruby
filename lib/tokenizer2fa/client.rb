require 'tokenizer2fa/token/state'
require 'tokenizer2fa/token/adapter/json'
require 'tokenizer2fa/token/adapter/xml'

require 'uri'
require 'net/http'

module Tokenizer2fa
  class Client
    attr_reader :app_id, :app_key, :host, :format

    DEFAULT_CREATE_AUTH_URL = '%{host}v1/authentications.%{format}'
    DEFAULT_USER_AUTH_URL = '%{host}v1/authentication/%{id}.%{format}?%{params}'

    def initialize config={}
      @format = 'json'
      config.each { |key,value| instance_variable_set("@#{key}",value) }
      begin
        @adapter = Object.class.const_get("Token::Adapter::#{@format.capitalize}").new
      rescue
        @format = 'json'
        @adapter = Token::Adapter::Json.new
      end
    end

    # Checks whether all required settings are set
    def validate_config
      @app_id && @app_key && @host
    end

    # Generates authentication token for an email address
    def create_auth email, url=DEFAULT_CREATE_AUTH_URL
      config_params = {
          app_id: @app_id,
          app_key: @app_key,
          usr_email: email  # notice the missing e in usr_email
      }

      uri = URI(url % { host: @host, format:  @format })
      https = Net::HTTP.new(uri.hostname, uri.port)
      https.use_ssl = true

      request = Net::HTTP::Post.new uri.request_uri
      request.set_form_data config_params
      request.content_type = 'application/x-www-form-urlencoded'

      response = https.request request
      @adapter.parse(response.body)['id']
    end

    # Checks whether token ID is valid
    def verify_auth id, url=DEFAULT_USER_AUTH_URL
      config_params = URI.encode_www_form({
                                              app_id:  @app_id,
                                              app_key: @app_key
                                          })

      uri = URI(url % { host: @host, id: id, format: @format, params: config_params })
      https = Net::HTTP.new(uri.hostname, uri.port)
      https.use_ssl = true

      request = Net::HTTP::Get.new uri.request_uri

      response = https.request(request)
      Token::State.new @adapter.parse(response.body)['state']
    end

    def poll_verify_auth id, url=DEFAULT_USER_AUTH_URL
      loop do
        state = verify_auth id, url
        return state unless state.is_pending?
        sleep 1
      end
    end
  end
end
